﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.Utils;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.BarCode;
using DevExpress.XtraReports.UI;

using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Linq.Expressions;
using System.Globalization;
using System.Xml.Serialization;

namespace BarcodeMaker
{
    public partial class rpt_Print : DevExpress.XtraReports.UI.XtraReport
    {

        private DataTable dt_items_qty = new DataTable();
        int currentID = 0;
        private BarcodeTemplate template;
        public rpt_Print()
        {
            InitializeComponent();
        }
        public rpt_Print ( BarcodeTemplate Template , DataTable table  )
        {
            this.InitializeComponent();
            if (Template.ColumnsCount <= 0) Template.ColumnsCount = 1;
            if (Template.RowsCount  <= 0) Template.RowsCount = 1;
            this.lbl_Line_1.BeforePrint += new PrintEventHandler(this.lbl_Line_1_BeforePrint);
            this.lbl_Line_2.BeforePrint += new PrintEventHandler(this.lbl_Line_2_BeforePrint);
            this.lbl_Line_3.BeforePrint += new PrintEventHandler(this.lbl_Line_3_BeforePrint);
            this.lbl_Line_4.BeforePrint += new PrintEventHandler(this.lbl_Line_4_BeforePrint);
            //this.StartFrom = start_from;
            this.dt_items_qty = table;
            
          
            this.template = Template;
            PaperKind paperKind = (PaperKind)Enum.Parse(typeof(PaperKind), this.template.PageName);
            this.PaperKind = paperKind;
            if (paperKind == PaperKind.Custom)
            {
                this.PageHeight = this.template.PaperHeight;
                this.PageWidth = this.template.PaperWidth;
            }
            this.xrPanel1.Borders = BorderSide.None;
            this.topMarginBand1.HeightF = (float)this.template.Margins_Top;
            this.bottomMarginBand1.HeightF = (float)this.template.Margins_Bottom;
            this.Margins.Left = this.template.Margins_Left;
            this.Margins.Right = this.template.Margins_Right;
            this.Margins.Top = this.template.Margins_Top;
            this.Margins.Bottom = this.template.Margins_Bottom;
            this.Detail.MultiColumn.ColumnCount = (int)this.template.ColumnsCount;
            this.Detail.MultiColumn.ColumnWidth = (float)((this.PageSize.Width - this.Margins.Left - this.Margins.Right) / (int)this.template.ColumnsCount);
            --this.Detail.MultiColumn.ColumnWidth;
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = (int)this.template.RowsCount * (int)this.template.ColumnsCount;
            this.Detail.HeightF = ((float)this.PageSize.Height - (float)this.Margins.Top - (float)this.Margins.Bottom) / (float)this.template.RowsCount;
            this.Detail.HeightF = (float)(int)this.Detail.HeightF;
            --this.Detail.HeightF;
            this.lbl_Line_4.ShowText = this.template.ShowBacrodeText;
            this.xrPanel1.HeightF = this.Detail.HeightF;
            this.xrPanel1.WidthF = this.Detail.MultiColumn.ColumnWidth;
            this.Print_BarCode_Labels();
        }
        CultureInfo info = new CultureInfo("AR-EG");
        string ReplaceSyntax(string str)
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd dddd", info);
            string time = DateTime.Now.ToString("hh:mm tt", info);
            string datetime = DateTime.Now.ToString("yyyy-MM-dd dddd hh:mm tt", info);
            str= str.Replace("@Date", date);
            str= str.Replace("@DateTime", datetime);
            str= str.Replace("@Time", time);
            return str;
        }
        private void Print_BarCode_Labels()
        {
            //Font font = this.lbl_Line_1.Font;
            //if (this.template.Font != null && this.template.Font != string.Empty)
            //    font = new Font(this.template.Font, font.Size);
            this.lbl_Line_1.Visible = this.template.Line_1_Visible;
            this.lbl_Line_2.Visible = this.template.Line_2_Visible;
            this.lbl_Line_3.Visible = this.template.Line_3_Visible;
            this.lbl_Line_4.Visible = this.template.Line_4_Visible;
            this.lbl_Line_1.Font    =  this.template.Line_1_Font.Font;
            this.lbl_Line_2.Font    =  this.template.Line_2_Font.Font;
            this.lbl_Line_3.Font    =  this.template.Line_3_Font.Font;
            this.lbl_Line_4.Font    =  this.template.Line_4_Font.Font;
            float num = 0.0f;
            if (this.lbl_Line_1.Visible)
            {
                this.lbl_Line_1.HeightF = this.lbl_Line_1.Font.Size * 5f;
                num += this.lbl_Line_1.HeightF;
            }
            if (this.lbl_Line_2.Visible)
            {
                this.lbl_Line_2.HeightF = this.lbl_Line_2.Font.Size * 5f;
                num += this.lbl_Line_2.HeightF;
            }
            if (this.lbl_Line_3.Visible)
            {
                this.lbl_Line_3.HeightF = this.lbl_Line_3.Font.Size * 5f;
                num += this.lbl_Line_3.HeightF;
            }
            this.lbl_Line_4.HeightF = (float)(((double)this.xrPanel1.HeightF - (double)num) * 0.800000011920929);
            this.lbl_Line_1.Width = this.lbl_Line_4.Width = this.lbl_Line_2.Width = this.lbl_Line_3.Width = (int)this.xrPanel1.WidthF * 8 / 10;


            this.lbl_Line_1.Text = ReplaceSyntax(template.Line_1);
            this.lbl_Line_2.Text = ReplaceSyntax(template.Line_2);
            this.lbl_Line_3.Text = ReplaceSyntax(template.Line_3);
            
            this.DataSource = (object)dt_items_qty;
            this.lbl_Line_4.DataBindings.Add("Text", this.DataSource, "Barcode");

        }


        private Font ChangeFontSize(Font font, float fontSize)
        {
            if (font != null && (double)fontSize > 0.0 && (double)font.Size != (double)fontSize)
                font = new Font(font.Name, fontSize, font.Style, font.Unit, font.GdiCharSet, font.GdiVerticalFont);
            return font;
        }

    
   

        private void lbl_Line_1_BeforePrint(object sender, PrintEventArgs e)
        {
            if (!this.lbl_Line_1.Visible)
                e.Cancel = true;
            else
                this.lbl_Line_1.LocationF = new PointF(30f, 5f);
        }

        private void lbl_Line_2_BeforePrint(object sender, PrintEventArgs e)
        {
            if (!this.lbl_Line_2.Visible)
                e.Cancel = true;
            if (!this.lbl_Line_1.Visible)
                this.lbl_Line_2.LocationF = new PointF(30f, 5f);
            else
                this.lbl_Line_2.LocationF = new PointF(30f, this.lbl_Line_1.LocationF.Y + this.lbl_Line_1.HeightF);
        }

        private void lbl_Line_3_BeforePrint(object sender, PrintEventArgs e)
        {
            if (!this.lbl_Line_3.Visible)
                e.Cancel = true;
            if (!this.lbl_Line_2.Visible && !this.lbl_Line_1.Visible)
                this.lbl_Line_3.LocationF = new PointF(30f, 5f);
            else if (!this.lbl_Line_2.Visible)
                this.lbl_Line_3.LocationF = new PointF(30f, this.lbl_Line_1.LocationF.Y + this.lbl_Line_1.HeightF);
            else if (!this.lbl_Line_1.Visible)
                this.lbl_Line_3.LocationF = new PointF(30f, this.lbl_Line_2.LocationF.Y + this.lbl_Line_2.HeightF);
            else
                this.lbl_Line_3.LocationF = new PointF(30f, this.lbl_Line_2.LocationF.Y + this.lbl_Line_2.HeightF);
        }

        private void lbl_Line_4_BeforePrint(object sender, PrintEventArgs e)
        {
            if (!this.lbl_Line_4.Visible)
                e.Cancel = true;
            if (!this.lbl_Line_2.Visible && !this.lbl_Line_1.Visible && !this.lbl_Line_3.Visible)
                this.lbl_Line_4.LocationF = new PointF(30f, 5f);
            else if (!this.lbl_Line_2.Visible && !this.lbl_Line_3.Visible)
                this.lbl_Line_4.LocationF = new PointF(30f, this.lbl_Line_1.LocationF.Y + this.lbl_Line_1.HeightF);
            else if (!this.lbl_Line_3.Visible)
                this.lbl_Line_4.LocationF = new PointF(30f, this.lbl_Line_2.LocationF.Y + this.lbl_Line_2.HeightF);
            else
                this.lbl_Line_4.LocationF = new PointF(30f, this.lbl_Line_3.LocationF.Y + this.lbl_Line_3.HeightF);
        }

        private void xrPanel1_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
           
            //e.Cancel = true;
        }

    }
    
    public class BarcodeTemplate  
    {
        public BarcodeTemplate()
        {
            ColumnsCount = RowsCount = 1;
            Line_1 =  Line_2 = Line_3 =  Line_4 = "";
            Line_1_Font = new SerializableFont();
            Line_2_Font = new SerializableFont();
            Line_3_Font = new SerializableFont();
            Line_4_Font = new SerializableFont();
              
            Line_1_Visible =Line_2_Visible =  Line_3_Visible =  Line_4_Visible = true;
            PageName = "Custom";
            PaperWidth = PaperHeight = 250;
            Printer =
            lastInt = "";
        }
     
    
        public byte   ColumnsCount;
        public byte   RowsCount;
 
        public string Line_1;
        public string Line_2;
        public string Line_3;
        public string Line_4;
        public SerializableFont Line_1_Font;
        public SerializableFont Line_2_Font;
        public SerializableFont Line_3_Font;
        public SerializableFont Line_4_Font;
        public bool   Line_1_Visible;
        public bool   Line_2_Visible;
        public bool   Line_3_Visible;
        public bool   Line_4_Visible;
        public string PageName;
        public int    PaperWidth;
        public int    PaperHeight;
        public int    Margins_Top;
        public int    Margins_Bottom;
        public int    Margins_Left;
        public int    Margins_Right;
        public bool   ShowBacrodeText;
        public string Printer;
        public string lastInt; 

      
    
   
    
    }


    public sealed class SerializableFont
    {
        // public properties are required for serialization only
        public string FamilyName;
        public float EmSize;
        public FontStyle style = FontStyle.Regular;

        // required for serializaton only
        public SerializableFont()
        {
            FontStyle style = FontStyle.Regular;
            Font = new Font("Times New Roman", 8);
            

        }

        public SerializableFont(Font font)
        {
            FamilyName = font.FontFamily.Name;
            EmSize = font.Size;
            style = font.Style;
        }

        [XmlIgnore]
        public Font Font
        {
            get
            {
                
                return new Font(FamilyName, EmSize,style );
            }
            set
            {
                FamilyName = value.FontFamily. Name ;
                EmSize = value.Size;
                style = value.Style;
            }
        }
    }

}
