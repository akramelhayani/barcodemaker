﻿using DevExpress.XtraEditors.Controls;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace BarcodeMaker
{
    public partial class frm_Barcode : DevExpress.XtraEditors.XtraForm 
    {
        public frm_Barcode()
        {
            InitializeComponent();
        }
        public frm_Barcode(string caller)
        {
            InitializeComponent();
            IsFormStarted = false;
            outCalled = true;
            FilePath = caller;   
        }
        rpt_Print report = new rpt_Print();
        Image LableImage;
        string  FilePath;
        bool IsNew,IsSaved,outCalled;
         
        BarcodeTemplate barcodeTemplate = new BarcodeTemplate();
        bool IsFormStarted = false;
        private void frm_Barcode_Load(object sender, EventArgs e)
        {
            spinEdit5.EditValueChanged  += DataChanged;
            spinEdit6.EditValueChanged  += DataChanged;
          
            foreach (int num in Enum.GetValues(typeof(PaperKind)))
                this.cb_Papers.Properties.Items.Add(new ImageComboBoxItem(Enum.GetName(typeof(PaperKind), (object)num), (object)Enum.GetName(typeof(PaperKind), (object)num), -1));
            foreach (FontFamily oneFontFamily in FontFamily.Families)
                cb_Fonts.Properties.Items.Add(oneFontFamily.Name);
            foreach (String strPrinter in PrinterSettings.InstalledPrinters)
                comboBoxEdit1.Properties .Items.Add(strPrinter);
            if(!outCalled )
            btn_New.PerformClick();
            else load(FilePath);
            IsFormStarted = true;
        }
        void UpdateTimplate()
        {
            barcodeTemplate.ColumnsCount =Convert.ToByte( txtColumnsCount.EditValue);
            //barcodeTemplate.Font = cb_Fonts.EditValue.ToString();
            barcodeTemplate.Line_1 = textEdit1.Text;
            barcodeTemplate.Line_2 = textEdit2.Text;
            barcodeTemplate.Line_3 = textEdit3.Text;
            barcodeTemplate.Line_4 = barCodeControl1.Text;
            //barcodeTemplate.Line_1_Font =Convert.ToDouble(  spinEdit1.EditValue);
            //barcodeTemplate.Line_2_Font =Convert.ToDouble( spinEdit2.EditValue);
            //barcodeTemplate.Line_3_Font =Convert.ToDouble( spinEdit3.EditValue);
            //barcodeTemplate.Line_4_Font =Convert.ToDouble( spinEdit4.EditValue);
            barcodeTemplate.Line_1_Visible = checkEdit1.Checked; 
            barcodeTemplate.Line_2_Visible = checkEdit2.Checked; 
            barcodeTemplate.Line_3_Visible = checkEdit3.Checked; 
            barcodeTemplate.Line_4_Visible = checkEdit4.Checked;
            barcodeTemplate.ShowBacrodeText = checkEdit5.Checked;
            barcodeTemplate.Margins_Bottom = Convert.ToInt32( txt_MarginBottom.EditValue);
            barcodeTemplate.Margins_Left = Convert.ToInt32(txt_MarginLeft.EditValue);
            barcodeTemplate.Margins_Right = Convert.ToInt32(txt_MarginRight.EditValue);
            barcodeTemplate.Margins_Top = Convert.ToInt32(txt_MarginTop.EditValue);
            barcodeTemplate.PageName = cb_Papers.Text;
            barcodeTemplate.PaperHeight = Convert.ToInt32(txt_Paperheight.EditValue) ;
            barcodeTemplate.PaperWidth  = Convert.ToInt32( txt_Paperwidth.EditValue );
            barcodeTemplate.RowsCount = Convert.ToByte(txtRowsCount.EditValue);
            barcodeTemplate.ShowBacrodeText = checkEdit5.Checked;
            barcodeTemplate.Printer = comboBoxEdit1.Text;
            barcodeTemplate.lastInt = spinEdit6.EditValue.ToString();

        }
     
        void PreviewReport()
        {
            Stream stream = new System.IO.MemoryStream();
            report.ExportToImage(stream:stream) ;
            LableImage = System.Drawing.Image.FromStream(stream);
            pictureBox2.Image = System.Drawing.Image.FromStream(stream);

        }

        private void spinEdit5_EditValueChanged(object sender, EventArgs e)
        {
            barCodeControl1.Text = spinEdit5.Text.ToString();
        }

     

        private void DataChanged(object sender, EventArgs e)
        {
            if (!IsFormStarted) return;
            IsSaved = false;
            barStaticItem2.Caption = "Not Saved";
            PreviewImage();
        }
        void PreviewImage()
        {
            UpdateTimplate();
            DataTable table = new DataTable();
            int count = 1;
            if ((Convert.ToInt32(txtColumnsCount.EditValue) + (Convert.ToInt32(txtRowsCount.EditValue))) > 0) count = (Convert.ToInt32(txtColumnsCount.EditValue) * (Convert.ToInt32(txtRowsCount.EditValue)));
                
                table.Columns.Add("Barcode");
            for (int i = 0; i < count; i++)
            {
            table.Rows.Add(spinEdit5.EditValue);
            }
            report = new rpt_Print(barcodeTemplate, table);
            PreviewReport();
        }

        bool IsSpinEdit6Focused;
        bool IsSpinEdit7Focused;
        private void spinEdit6_EditValueChanged(object sender, EventArgs e)
        {
            if (IsSpinEdit6Focused)
            {
                spinEdit7.EditValue =  Convert.ToInt32(spinEdit6.EditValue)-Convert.ToInt32(spinEdit5.EditValue) ;
            }
        }

        private void spinEdit7_EditValueChanged(object sender, EventArgs e)
        {
            if (IsSpinEdit7Focused)
            {
              
                spinEdit6.EditValue = Convert.ToInt32(spinEdit5.EditValue) + Convert.ToInt32(spinEdit7.EditValue);
            }
        }

        private void spinEdit5_Enter(object sender, EventArgs e)
        {

        }

        private void spinEdit6_Enter(object sender, EventArgs e)
        {
            IsSpinEdit6Focused = true;

        }

        private void spinEdit7_Enter(object sender, EventArgs e)
        {
            IsSpinEdit7Focused = true ;
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            UpdateTimplate();
            DataTable table = new DataTable();
            table.Columns.Add("Barcode");
            for (int i = Convert.ToInt32(spinEdit5.EditValue); i < Convert.ToInt32(spinEdit6.EditValue); i++)
            {
                table.Rows.Add(i);
            }
            report = new rpt_Print(barcodeTemplate, table);
            using (ReportPrintTool printtool = new ReportPrintTool(report))
            {
                printtool.PrinterSettings.PrinterName = comboBoxEdit1.Text;
                printtool.ShowRibbonPreviewDialog();
            }
        }

        private void btn_Save_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            UpdateTimplate();
            if (!IsNew && FilePath.Trim()!="")
            {
                XmlSerializer xs = new XmlSerializer(typeof(BarcodeTemplate));
                TextWriter tw = new StreamWriter(FilePath);
                xs.Serialize(tw, barcodeTemplate);
                IsSaved = true;
                barStaticItem2.Caption = "Saved";

            }
            else
            {
                SaveFileDialog op = new SaveFileDialog();
                op.Filter = "Barcode  File (*.bar)|*.bar";
                op.FilterIndex = 1;
                op.RestoreDirectory = true;

                op.DefaultExt = "bar";
                op.AddExtension = true;
                if (op.ShowDialog() == DialogResult.OK && op.FileName.Trim() != "")
                {
                    XmlSerializer xs = new XmlSerializer(typeof(BarcodeTemplate));
                    TextWriter tw = new StreamWriter(op.FileName);
                    xs.Serialize(tw, barcodeTemplate);
                    IsNew = true;
                    FilePath = op.FileName;
                    IsSaved = true;
                    barStaticItem1.Caption = FilePath;
                    barStaticItem2.Caption = "Saved";
                }
              

            }

        }
        
        private void btn_New_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            IsFormStarted = false;
            barcodeTemplate = new BarcodeTemplate();
            LoadTempletData();
            FilePath = "";
            IsNew = true;
            IsSaved = true ;
            barStaticItem2.Caption = "New File";
            IsFormStarted = true ;

        }
        void LoadTempletData()
        {
            txtColumnsCount.EditValue = barcodeTemplate.ColumnsCount;
            //cb_Fonts.EditValue = barcodeTemplate.Font;
            textEdit1.Text = barcodeTemplate.Line_1;
            textEdit2.Text = barcodeTemplate.Line_2;
            textEdit3.Text = barcodeTemplate.Line_3;
            barCodeControl1.Text = barcodeTemplate.Line_4;
            //spinEdit1.EditValue = barcodeTemplate.Line_1_Font;
            //spinEdit2.EditValue = barcodeTemplate.Line_2_Font;
            //spinEdit3.EditValue = barcodeTemplate.Line_3_Font;
            //spinEdit4.EditValue = barcodeTemplate.Line_4_Font;
            checkEdit1.Checked = barcodeTemplate.Line_1_Visible;
            checkEdit2.Checked = barcodeTemplate.Line_2_Visible;
            checkEdit3.Checked = barcodeTemplate.Line_3_Visible;
            checkEdit4.Checked = barcodeTemplate.Line_4_Visible;
            checkEdit5.Checked = barcodeTemplate.ShowBacrodeText;
            txt_MarginBottom.EditValue = barcodeTemplate.Margins_Bottom;
            txt_MarginLeft.EditValue = barcodeTemplate.Margins_Left;
            txt_MarginRight.EditValue = barcodeTemplate.Margins_Right;
            txt_MarginTop.EditValue = barcodeTemplate.Margins_Top;
            cb_Papers.Text = barcodeTemplate.PageName;
            txt_Paperheight.EditValue = barcodeTemplate.PaperHeight;
            txt_Paperwidth.EditValue = barcodeTemplate.PaperWidth;
            txtRowsCount.EditValue = barcodeTemplate.RowsCount;
            checkEdit5.Checked = barcodeTemplate.ShowBacrodeText;
            comboBoxEdit1.Text = barcodeTemplate.Printer;
            spinEdit6.EditValue = barcodeTemplate.lastInt;
            barStaticItem1.Caption = FilePath;
        }

        private void btn_Open_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "Barcode  File (*.bar)|*.bar";
            op.FilterIndex = 1;
            op.RestoreDirectory = true;
            op.DefaultExt = "bar";
            if(op.ShowDialog()== DialogResult.OK && op.FileName .Trim() != "")
            {
                load(op.FileName);
            }
           

        }
        void load(string file)
        {
            IsFormStarted = false;

            if (System.IO.Path.GetExtension(file).Equals(".bar"))
            {

                XmlSerializer xs = new XmlSerializer(typeof(BarcodeTemplate));
                using (var sr = new StreamReader(file ))
                {
                    barcodeTemplate = (BarcodeTemplate)xs.Deserialize(sr);
                }
                FilePath = file;
                IsNew = false;
                IsSaved = true;
                barStaticItem2.Caption = "No Changes";

                LoadTempletData();
                PreviewImage();
            }
            else
                MessageBox.Show("Unrecognized File");
            IsFormStarted = true;

        }

        private void frm_Barcode_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSaved)
            {
                switch (DevExpress.XtraEditors.XtraMessageBox.Show("يوجد بيانات لم يتم حفظها "+ Environment.NewLine +" هل تريد الحفظ اولا ", " تحذير ", MessageBoxButtons.YesNoCancel , MessageBoxIcon.Exclamation))
                {
                    case DialogResult.Yes :
                        btn_Save.PerformClick();
                        break;
                    case DialogResult.No :
                        
                        break;
                    case DialogResult.Cancel :
                        e.Cancel = true;
                       
                        break;
                }
               
            }
        }

        private void spinEdit6_Leave(object sender, EventArgs e)
        {
            IsSpinEdit6Focused = false ;
        }

        private void spinEdit7_Leave(object sender, EventArgs e)
        {
            IsSpinEdit7Focused = false ;
        }
        CultureInfo info = new CultureInfo("AR-EG");
        private void ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripItem item = sender as ToolStripItem;
            if (item == null) return;
            var insertText = item.Tag.ToString ();
            var selectionIndex = ((DevExpress.XtraEditors.TextBoxMaskBox)contextMenuStrip1.SourceControl).OwnerEdit.SelectionStart;
            ((DevExpress.XtraEditors.TextBoxMaskBox)contextMenuStrip1.SourceControl).OwnerEdit.Text = ((DevExpress.XtraEditors.TextBoxMaskBox)contextMenuStrip1.SourceControl).OwnerEdit.Text.Insert(selectionIndex, insertText);
            ((DevExpress.XtraEditors.TextBoxMaskBox)contextMenuStrip1.SourceControl).OwnerEdit.SelectionStart = selectionIndex + insertText.Length;
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = barcodeTemplate.Line_1_Font.Font;
            if (fontDialog1.ShowDialog() == DialogResult.OK)
                barcodeTemplate.Line_1_Font.Font = fontDialog1.Font;
            DataChanged(sender, e);
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = barcodeTemplate.Line_2_Font.Font;

            if (fontDialog1.ShowDialog() == DialogResult.OK)
                barcodeTemplate.Line_2_Font.Font = fontDialog1.Font;
            DataChanged(sender, e);

        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            fontDialog1.Font = barcodeTemplate.Line_3_Font.Font;

            if (fontDialog1.ShowDialog() == DialogResult.OK)
                barcodeTemplate.Line_3_Font.Font = fontDialog1.Font;
            DataChanged(sender, e);

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
           
        }

    

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if ((string.IsNullOrEmpty(comboBoxEdit1.Text))) return;
            UpdateTimplate();
            DataTable table = new DataTable();
            table.Columns.Add("Barcode");
            for (int i = Convert.ToInt32(spinEdit5.EditValue); i < Convert.ToInt32(spinEdit6.EditValue); i++)
            {
                table.Rows.Add(i);
            }
            report = new rpt_Print(barcodeTemplate, table);
            using (ReportPrintTool printtool = new ReportPrintTool(report))
            {
               
                printtool.Print(comboBoxEdit1.Text);
            }


        }
    }
}
